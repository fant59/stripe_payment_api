# STRIPE API

[![Django](https://img.shields.io/badge/-Django-464646?style=flat-square&logo=Django)](https://www.djangoproject.com/)
[![Nginx](https://img.shields.io/badge/-NGINX-464646?style=flat-square&logo=NGINX)](https://nginx.org/ru/)
[![docker](https://img.shields.io/badge/-Docker-464646?style=flat-square&logo=docker)](https://www.docker.com/)

Проект для тестирования электронных платежей Stripe.

## Методы API:
```
GET /item/{id}
```
Получает описание товара. При нажатии на кнопку Buy будет выполнена тестовая покупка товара.

```
GET /buy/{id}
```
Формирует тестовый Stripe Session Id для оплаты выбранного товара.


## Запуск проекта в контейнерах Docker

Клонируем репозиторий

```bash
git clone https://gitlab.com/fant59/stripe_payment_api.git
```
Создаем в корневой директории файл с параметрами рабочего окружения
```bash
touch .env
```
Заполнить указанные переменные соответствующими ключами.
```
DJANGO_SECRET_KEY=your_django_secret_key
STRIPE_SECRET_KEY=your_stripe_secret_key
```

Запуск приложения:
```bash
docker-compose up -d --build
```
Создать суперюзера, статика и миграции выполняются при развертывании docker-compose:
```bash
docker-compose exec backend python manage.py createsuperuser
```
Остановить контейнеры

```bash
docker-compose down -v
```

## Создание тестовых товаров

После того как будет создан суперюзер:
1. Зайти в админку сайта
1. Создать несколько товаров
1. Протестировать работоспособность `api`

## Flow-покупки товара
Для выполнения успешной покупки выбранного товара, необходимо заполнить: 

- почту
- имя
- омер карты должен быть равен `4242 4242 4242 4242`
- срок окончания
- cvv-код

Все параметры за исключением номера карты могут быть любыми.
