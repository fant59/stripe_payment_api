FROM python:3.8-slim-buster

RUN mkdir /app

COPY requirements.txt /app

RUN pip3 install -r /app/requirements.txt --no-cache-dir

COPY stripe_api/ /app
COPY entrypoint.sh /app

WORKDIR /app

ENTRYPOINT ["sh", "entrypoint.sh"]