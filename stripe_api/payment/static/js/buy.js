(
    function () {
        var stripe = Stripe("pk_test_51LlZP7GsVCKGoXUEs0V8BlXHEOME3Z0pemHhhaJ0c7oHSOUJGgHE802PZCVLXjm6vbVTCvv2XSQjOvNT2asl0f9v005WszORwD");

        function buy(url) {
            let buyButton = document.getElementById('buy-button');
            buyButton.addEventListener('click', function () {
                fetch(
                    url, {method: "GET"}
                )
                    .then(function (response) {
                        return response.json()
                    })
                    .then(function (session) {
                        stripe.redirectToCheckout({sessionId: session.id});
                    })
            });

        };
        window.buy = buy;
})();