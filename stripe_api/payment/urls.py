from django.urls import (
    path,
)

from .views import (
    ApiItem,
    ApiPayment,
    CancelPaymentView,
    index,
    SuccessPaymentView,
)

app_name = 'payment'

#
urlpatterns = [
    path('', index),
    path('item/<int:pk>', ApiItem.as_view(), name='item-detail'),
    path('buy/<int:pk>', ApiPayment.as_view(), name='item-buy'),
    path('success/', SuccessPaymentView.as_view()),
    path('cancel/', CancelPaymentView.as_view()),
]
