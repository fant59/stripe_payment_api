import os

from django.http import (
    JsonResponse,
    HttpResponseRedirect,
)
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView
from dotenv import load_dotenv
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
import stripe

from .models import Item
from .serializers import ItemSerializer

load_dotenv()

stripe.api_key = os.environ.get('STRIPE_SECRET_KEY')


def index(request):
    return HttpResponseRedirect('/item/1')


class ApiPayment(APIView):
    def get(self, request, pk):
        item = get_object_or_404(Item, pk=pk)
        session = stripe.checkout.Session.create(
            line_items=[{
                'price_data': {
                    'currency': 'rub',
                    'unit_amount': item.stripe_price,
                    'product_data': {
                        'name': item.name
                    }
                },
                'quantity': 1
            }],
            mode='payment',
            success_url=request.build_absolute_uri('/success/'),
            cancel_url=request.build_absolute_uri('/cancel/')
        )
        return JsonResponse({'id': session.id})


class ApiItem(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'payment/item_detail.html'

    def get(self, request, pk):
        item = get_object_or_404(Item, pk=pk)
        serializer = ItemSerializer(item)
        return Response({'serializer': serializer, 'item': item})


class SuccessPaymentView(TemplateView):
    template_name = 'payment/success.html'


class CancelPaymentView(TemplateView):
    template_name = 'payment/cancel.html'
