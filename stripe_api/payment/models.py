from django.db import models


class Item(models.Model):
    """Model items to buy them"""
    name = models.CharField(verbose_name='Название продукта', unique=True, max_length=25)
    description = models.CharField(verbose_name='Описание', max_length=250)
    price = models.PositiveIntegerField(verbose_name='Стоимость')

    def __str__(self):
        return self.name

    @property
    def stripe_price(self):
        return self.price * 100
